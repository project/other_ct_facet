<?php

namespace Drupal\other_ct_facet\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Define Class DefaultForm.
 */
class DefaultForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'other_ct_facet.default',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'default_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $types = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();

    $options = [];

    foreach ($types as $key => $type) {
      $options[$key] = $type->label();
    }

    $config = $this->config('other_ct_facet.default');
    $form['content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content Types'),
      '#description' => $this->t('Select the contenttypes that should be indexed as &#039;Other&#039;'),
      '#options' => $options,
      '#default_value' => $config->get('content_types'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('other_ct_facet.default')
      ->set('content_types', $form_state->getValue('content_types'))
      ->save();

    $otherConfig = \Drupal::config('other_ct_facet.default')->get('content_types');
    $otherContentTypes = [];
    foreach ($otherConfig as $value) {
      if ($value !== 0) {
        $otherContentTypes[] = $otherConfig;
      }
    }
  }

}
