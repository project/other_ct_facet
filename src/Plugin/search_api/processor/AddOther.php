<?php

namespace Drupal\other_ct_facet\Plugin\search_api\processor;

/**
 * @file
 * Processor plugin.
 *
 * This file provides a custom search api processor plugin.
 */


use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Plugin\search_api\processor\Property\AggregatedFieldProperty;

/**
 * Processor plugin.
 *
 * Adds the node's contenttype to the indexed data.
 * And clusters 'selected contenttypes' to 'Other'
 *
 * @SearchApiProcessor(
 *   id = "add_other",
 *   label = @Translation("Other entity type ID field"),
 *   description = @Translation("Adds the item's EntityType Other to the
 *   index."), stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class AddOther extends ProcessorPluginBase {

  /**
   * The contenttypes that should be indexed as 'Other'.
   *
   * @var string[]
   */
  public function getOtherContentTypes() {
    $otherConfig = \Drupal::config('other_ct_facet.default')->get('content_types');
    return array_values($otherConfig);
  }

  /**
   * Retrieves the properties this processor defines for the given datasource.
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Other contenttype'),
        'description' => $this->t('Marks specified contenttype as Other'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['search_api_other'] = new AggregatedFieldProperty($definition);
    }

    return $properties;
  }

  /**
   * Populates the Defined property with field with values.
   *
   * The value is equal to the contenttype or bundle.
   * But certain contenttypes values are marked as other.
   */
  public function addFieldValues(ItemInterface $item) {
    $datasourceId = $item->getDatasourceId();
    if ($datasourceId == 'entity:node') {
      $node = $item->getOriginalObject()->getValue();
      $nodeType = $node->bundle();
      // A nodeType was found, add it to the relevant field.
      $fields = $this->getFieldsHelper()
        ->filterForPropertyPath($item->getFields(), NULL, 'search_api_other');

      foreach ($fields as $field) {
        if (in_array($nodeType, $this->getOtherContentTypes())) {
          $field->addValue('Other');
        }
        else {
          $field->addValue($nodeType);
        }
      }
    }
  }

}
